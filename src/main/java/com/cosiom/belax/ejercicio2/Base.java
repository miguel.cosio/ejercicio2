package com.cosiom.belax.ejercicio2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

public class Base {
	public WebDriver driver;
	
	public WebDriver getmydrive() throws IOException{
		Properties prop= new Properties();
		FileInputStream fis= new FileInputStream("C:\\Users\\miguel.cosio\\workspace\\ejercicio2\\src\\main\\java\\configuration.properties");//C:\Users\miguel.cosio\workspace\ejercicio2\src\main\java\com\cosiom\belax\ejercicio2\Base.java
		prop.load(fis);
		String browser=prop.getProperty("browser");
		fis.close();
		switch(browser){
		case "Firefox":
			driver=Firefox.inicializaFirefoxDriver();
			break;
		case "Chrome":
			driver=Chrome.inicializaChromeDriver();
			break;
		case "IExplorer":
			driver= IExplorer.inicializaIExplorerDriver();
			break;
		case "Safari":
			driver= Safari.inicializaSafariDriver();
			break;
		default:
			break;
		}
		return driver;
	}

}
