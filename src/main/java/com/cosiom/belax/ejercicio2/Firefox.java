package com.cosiom.belax.ejercicio2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Firefox {

	public static WebDriver inicializaFirefoxDriver() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver", "C:\\tools\\geckodriver-v0.18.0-win64\\geckodriver.exe");
		WebDriver driver= new FirefoxDriver();
		return driver;
	}

}
