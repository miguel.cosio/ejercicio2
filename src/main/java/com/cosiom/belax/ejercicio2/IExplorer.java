package com.cosiom.belax.ejercicio2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class IExplorer {

	public static WebDriver inicializaIExplorerDriver() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.ie.driver", "C:\\tools\\IEDriverServer_Win32_2.53.1\\IEDriverServer.exe");
		WebDriver driver= new InternetExplorerDriver();
		return driver;
	}

}
